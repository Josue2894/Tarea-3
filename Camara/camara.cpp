

#include <iostream>
#include <memory>
#include "camara.hpp"

void insert_shutter(const std::shared_ptr<Camara> &nodoCamara)
{
    std::shared_ptr<Camara> nodo_tmp = nodoCamara;

    if (nodoCamara == nullptr)
    {
        nodo_tmp->valor = 1;
        std::cout << "Añadiendo primer disparo" << std::endl;
        return;
    }
    // nodo_tmp = copia de ptr lista para recorrer lista y no perder ref original
   


    // Llegar a último nodo
    while (nodo_tmp->nodo_next != nullptr)
    {                                   // si siguiente nodo es null, llego al final
        nodo_tmp = nodo_tmp->nodo_next; // apunta a siguiente nodo
        // std::cout << "Recorriendo nodos de lista" << std::endl;
    }
    // Inserta dato al final de lista
    nodo_tmp->nodo_next = std::make_shared<Camara>(); // crea (hace espacio en memoria) el nodo next
    nodo_tmp->nodo_next->valor = 1;
    std::cout << "Añadiendo " << nodo_tmp->nodo_next->valor << " disparo" << std::endl;
}

int calculate_shutter_count(const std::shared_ptr<Camara> nodoCamara)
{
    int counter = 0;
    if (nodoCamara == nullptr)
    {
        return 0;
    }
    // nodo_tmp = copia de ptr lista para recorrer lista y no perder ref original
    std::shared_ptr<Camara> nodo_tmp = nodoCamara;

    // Llegar a último nodo
    while (nodo_tmp->nodo_next != nullptr)
    {                                   // si siguiente nodo es null, llego al final
        nodo_tmp = nodo_tmp->nodo_next; // apunta a siguiente nodo
        counter++;
    }
    return counter;
}
