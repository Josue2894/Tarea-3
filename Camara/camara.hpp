#pragma once

#include <memory> 

struct Camara;  // declaracion de nodo para lista

struct Camara {
    int valor;  // nodo almacena enteros
    int year;
    char brand;
    std::shared_ptr<Camara> nodo_next = nullptr;  // puntero a otro nodo
    
};


/**
 * @brief inserta un disparo 
 * @param nodoCamara Lista a modificar
*/
void insert_shutter(const std::shared_ptr<Camara> &nodoCamara); 

/**
 * @brief calcula la cuenta de disparos en la lista 
 * @param nodoCamara lista a revisar
 * @return Retorna el numero de entero de disparos 
*/
int calculate_shutter_count(const std::shared_ptr<Camara> nodoCamara);  




