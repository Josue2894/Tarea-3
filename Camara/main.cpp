#include <iostream>
#include <memory>
#include "camara.hpp"

int main()
{
    
 std::cout << "\nInicio de Ejercicio 1 - Tarea 3\n" << std::endl;

    auto Canon = std::make_shared<Camara>(); // hace shared pointer de struct Camara espacio en memoria para lista
    std::weak_ptr<Camara> weakptr; // hace weak point para monitorear si todavia existe el struct
    weakptr = Canon;    //enlaca wekptr con camara

    //lista->valor = 4;

    int limite_disparos = 10;

    int contador = 0;

/*
El siguiente while ejecuta el insert_shutter hasta llega y en cada ciclo se revisa la cuenta actual.
Si se llega al limite de la cuenta se borra el puntero. El while corre siempre y cuando el puntero exista
*/


    while (weakptr.expired() == false)
    {
        insert_shutter(Canon); //se agrega un disparo nuevo 

        std::cout << "El contador de disparos es de: " << calculate_shutter_count(Canon) << "\n" << std::endl; 

        if (calculate_shutter_count(Canon) == limite_disparos)

        {
            std::cout << "Se llegó al limite de disparos" << "\n" << std::endl;

            Canon.reset();

            std::cout << "Se elimino el estruct de Canon" << "\n" << std::endl;
           
        }
    }

     std::cout << "FIN" << std::endl;

    return 0;
}