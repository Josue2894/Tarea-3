/*
La aplicación corre el menú en un while loop. Hay 5 opciones diferentes 
y se seleccionan con ingresar el numero de opcion.
Se tienen por default cargados 6 recordatorios diferentes para facilitar el testeo de la app
*/
#include <iostream>
#include <memory>
#include "todoapp.hpp"
using namespace Estructura;

int main()
{

    std::cout << "\nInicio de Ejercicio 2 - Tarea 3\n"
              << std::endl;

    auto RecordatorioLista = std::make_shared<Estructura::ListaRecordatorios>(); // hace shared pointer de struct Camara espacio en memoria para lista
    int corre = 1;

    while (corre == 1) 
    {

        int opcion;

        std::cout << "\n******************************************" << std::endl;
        std::cout << "*                 - MENU -               *" << std::endl;
        std::cout << "*   1 - Ingresar recordatorio            *" << std::endl;
        std::cout << "*   2 - Mostrar todos los recordatorios  *" << std::endl;
        std::cout << "*   3 - Buscar un recordatorio por ID    *" << std::endl;
        std::cout << "*   4 - Eliminar un recordatorio por ID  *" << std::endl;
        std::cout << "*   5 - Salir                            *" << std::endl;
        std::cout << "******************************************\n"
                  << std::endl;
        std::cout << "Acontiunacion escriba el número de opcion a elegir:\n"
                  << std::endl;

        std::cout << "--> ";
        std::cin >> opcion;

        std::cout << " " << std::endl;

        switch (opcion)
        {
        case 1: 
            RecordatorioLista->insertar_recordatorio(1, "Ver partido", "Marzo-6", "20:00");
            RecordatorioLista->insertar_recordatorio(2, "Dormir", "Marzo-7", "21:00");
            RecordatorioLista->insertar_recordatorio(3, "Tomar café", "Marzo-8", "15:00");
            RecordatorioLista->insertar_recordatorio(4, "Dormir", "Marzo-9", "01:00");
            RecordatorioLista->insertar_recordatorio(5, "Hacer ejercicio", "Marzo-10", "22:00");
            RecordatorioLista->insertar_recordatorio(6, "Ir al doctor", "Marzo-11", "23:00");
            break;

        case 2: 
            RecordatorioLista->mostrar_recordatorios();
            break;

        case 3:

            int indice;
            std::cout << "Acontiunacion escriba el número de identificacion que desea ENCONTRAR:\n"<< std::endl;
            std::cout << "--> ";
            std::cin >> indice;
            std::cout << " " << std::endl;
            RecordatorioLista->buscar_recordatorio(indice);
            break;

        case 4:
            int indice1;
            std::cout << "Acontiunacion escriba el número de identificacion que desea BORRAR:\n"<< std::endl;
            std::cout << "--> ";
            std::cin >> indice1;
            std::cout << " " << std::endl;
            RecordatorioLista->eliminar_recordatorio(indice1);
            break;

        case 5:

            return 0;
            break;

        default:

            std::cout << "\nOpcion Invalida\n"
                      << std::endl;
            break;
        }
    }

    return 0;
}