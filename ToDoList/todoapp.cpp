

#include <iostream>
#include <memory>
#include "todoapp.hpp"

using namespace Estructura;


void ListaRecordatorios::insertar_recordatorio(int indentificador, const char *nota, const char *fecha, const char *hora)
{

    if (this->inicio_ == nullptr) //inserta recordatorio en el primer nodo si la lista no existe 
    {
        this->inicio_ = std::make_shared<ToDoList>(); 
        this->inicio_->ID = indentificador;
        this->inicio_->nota = nota;
        this->inicio_->fecha = fecha;
        this->inicio_->hora = hora;
        std::cout << "Añadiendo nota de ID " << this->inicio_->ID << " " << this->inicio_->nota << " " << this->inicio_->fecha << " " << this->inicio_->hora << std::endl;
        return;
    }

    std::shared_ptr<ToDoList> nodo_tmp = this->inicio_;

    while (nodo_tmp->nodo_next != nullptr) // apunta a siguiente nodo
    {                                   
        nodo_tmp = nodo_tmp->nodo_next; 
    }

    // Inserta dato al final de lista
    nodo_tmp->nodo_next = std::make_shared<ToDoList>(); // crea (hace espacio en memoria) el nodo next
    nodo_tmp->nodo_next->ID = indentificador;
    nodo_tmp->nodo_next->nota = nota;
    nodo_tmp->nodo_next->fecha = fecha;
    nodo_tmp->nodo_next->hora = hora;
    std::cout << "Añadiendo nota de ID " << nodo_tmp->nodo_next->ID << " " << nodo_tmp->nodo_next->nota << " " << nodo_tmp->nodo_next->fecha << " " << nodo_tmp->nodo_next->hora << std::endl;
}

void ListaRecordatorios::mostrar_recordatorios()
{
    int counter = 0;
    
    if (this->inicio_ == nullptr)
    {
        std::cout << "No existen recordatorios" << std::endl;
        return;
    }

    std::shared_ptr<ToDoList> nodo_tmp = this->inicio_;

    std::cout << "____________________________________" << std::endl;
    std::cout << "Identificador | Nota | Fecha | Hora\n"<< std::endl;

   
    while (nodo_tmp != nullptr)  // Imprime en pantalla los datos de todos los recordatorios hasta llegar al último
    { 
        std::cout << nodo_tmp->ID << " | " << nodo_tmp->nota << " | " << nodo_tmp->fecha << " | " << nodo_tmp->hora << std::endl;
        nodo_tmp = nodo_tmp->nodo_next; // apunta a siguiente nodo
    }
    std::cout << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
}

void ListaRecordatorios::buscar_recordatorio(int indentificador)
{
    int counter = 0;

    if (this->inicio_ == nullptr)
    {
        std::cout << "No existen recordatorios" << std::endl;
        return;
    }
    
    std::shared_ptr<ToDoList> nodo_tmp = this->inicio_;

    
    while (nodo_tmp != nullptr) //busca en todo los nodos existentes por un match con el ID ingresado y luego imprime el resultado 
    { 

        if (nodo_tmp->ID == indentificador)
        {
            std::cout << "___________________________________" << std::endl;
            std::cout << "Recordatorio encontrado con indice: " << nodo_tmp->ID << " encontrado --> " << nodo_tmp->nota << " - " << nodo_tmp->fecha << " - " << nodo_tmp->hora << std::endl;
            std::cout << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
            return;
        }

        nodo_tmp = nodo_tmp->nodo_next; // apunta a siguiente nodo
    }
    std::cout << "___________________________________" << std::endl;
    std::cout << "\nIndice no encontrado \n"
              << std::endl;
    std::cout << "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾" << std::endl;
}

void ListaRecordatorios::eliminar_recordatorio(int identificador)
{

    if (this->inicio_ == nullptr)
    {
        std::cout << "No existen recordatorios " << std::endl;

        return;
    }

    if (this->inicio_->ID == identificador) //sección para eliminar el PRIMER recordatorio
    {
        std::shared_ptr<ToDoList> nodo_matar1 = this->inicio_;
        this->inicio_ = nodo_matar1->nodo_next;
        nodo_matar1.reset();
        std::cout << "//*//*//* Se borró el recordatorio con identificador: " << identificador << " *\\*\\*\\" << std::endl;
        return;
    }

    std::shared_ptr<ToDoList> nodo_tmp = this->inicio_;

    while (nodo_tmp != nullptr) //sección para eliminar cualquier recordatorio después del primero
    { 
        if (nodo_tmp->nodo_next->ID == identificador)
        {
            std::cout << " Hay match de borrado" << std::endl;
            std::shared_ptr<ToDoList> nodo_matar = nodo_tmp->nodo_next;
            nodo_tmp->nodo_next = nodo_matar->nodo_next; 
            nodo_matar.reset();                          // mata nodo
            std::cout << "//*//*//* Se borró el recordatorio con identificador: " << identificador << " *\\*\\*\\" << std::endl;
            return;
        }
        nodo_tmp = nodo_tmp->nodo_next; // apunta a siguiente nodo
    }
    std::cout << "No hay " << identificador << "en la lista" << std::endl;
}
