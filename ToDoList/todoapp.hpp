#pragma once

#include <memory>

namespace Estructura
{

    struct ToDoList; // declaracion de nodo para lista enlazada

    /**
     * @brief Este es un nodo para la lista de recordatorios
     */
    struct ToDoList
    {
        int ID; // nodo almacena enteros
        const char *nota;
        const char *fecha;
        const char *hora;
        std::shared_ptr<ToDoList> nodo_next = nullptr; // puntero a nodo siguiente
    };

    class ListaRecordatorios
    {

    public:
        /**
         * @brief inserta un disparo
         * @param Lista Lista a modificar
         * @param identificador Numero Entero de identificador del recordatorio
         * @param nota Nota de recordatorio
         * @param fecha Fecha de recordatorio
         * @param hora Hora de recordatorio
         */
        // void insertar_recordatorio(int indentificador);
        void insertar_recordatorio(int indentificador, const char *nota, const char *fecha, const char *hora);

        /**
         * @brief muestra la totalidad de recordatorios en la lista
         */
        void mostrar_recordatorios();

        /**
         * @brief busca un recordatorio especifico usando el numero de identificador
         * @param identificador lista a revisar
         */
         void buscar_recordatorio(int identificador);

        /**
         * @brief elimina un recordatorio especifico usando el numero de identificador
         * @param Lista lista a revisar
         * @param indice identificador de recordatorio a ser borrado
         */
         void eliminar_recordatorio(int identificador);

        /**
         * Constructor
         */
        ListaRecordatorios()
        {
            this->inicio_ = nullptr;
        }

        /**
         * Destructor
         */
        ~ListaRecordatorios() {}

    private:
        std::shared_ptr<ToDoList> inicio_;
    };
}
